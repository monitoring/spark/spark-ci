# spark-ci

This project provides CI/CD in Marathon & Chronos for spark jobs.

It provides the main tools and skeletons for integrating app builds with deployments in Marathon and Chronos, taken into account production, QA and development environments.

## How to use it

In order to use it you will need to include the following bits in the .gitlab-ci.yaml file of the spark job's repository:

```
include: 'https://gitlab.cern.ch/monitoring/spark/spark-ci/raw/master/ci/nomad-ci.yaml'
```

Additionally, each orchestrator requires certain variables to be specified at project level. Not specifying any of these variables will result in the pipeline failing. These will be:
- `$NOMAD_TOKEN_DEV`
- `$NOMAD_TOKEN_GP`
- `$DIAGLAB_REGISTRY_TOKEN`

The CIs include a set of stages already predefined that the spark job should override:
- `app`: Used to build the Scala jars
- `docker_[dev|qa|stable]`: Used to generate and tag the docker image for the job

Also, the job repository will need to include the following mandatory files:
- `build.sbt`: Which should always be there as it's needed to build the Scala application
- `config/jobs.yml`: With specific configuration for the job application

### Configuration

The configuration files for Nomad should include specific configuration for the job.

There are a set of mandatory fields to declare:
- `app_version`: Should match the version in the build.sbt file, otherwise the pipeline will fail
- `jobs`: Jobs definitions (list) to be deployed in Nomad, it includes several subfields:
  - `job_name`: The name of the job as it should run in Nomad
  - `image`: The docker image to be used ("${DOCKER_IMAGE}" can be used as a placeholder for automatic replacement)
  - `job_type`: Either batch (for batch jobs) or service (for streaming jobs)
  - `cronjob_value`: In case it's a batch job, not needed otherwise
  - `entry_point`: Use the placeholder "${ENTRY_POINT}" so it's generated using the "command_line" field (for better readibility).
  - `command_line`: Extends the default commands with some extra ones; at least spark-submit is required and some constraints apply to its parameters:
    - `input-brokers-url` must be declared using the variable placeholder `${KAFKA_INPUT_BROKERS}`
    - `output-brokers-url` must be declared using the variable placeholder `${KAFKA_OUTPUT_BROKERS}`
    - `output-topic` must be declared using the variable placeholder `${KAFKA_TOPIC_PREFIX}` before the topic's name
- (optional) `check_kafka`: enables checking kafka brokers and topics are specified; can be "true" or "false" (defaults to true)

Moreover, all of the variables defined in the [environment's defaults](https://gitlab.cern.ch/monitoring/spark/spark-ci/-/tree/master/marathoncli/scripts/resources/defaults) may be used in this file.
What is more, you can also override any other field in the [skeletons](https://gitlab.cern.ch/monitoring/spark/spark-ci/-/tree/master/marathoncli/scripts/resources/skeletons), being the most common ones:
- `mem`: default depends on the configuration environment
- `cpus`: default 1
- `disk`: default 0
- `gpus`: default 0
- `instances`: default 0
- `spark_version`: default 2.3.2

#### Kafka Brokers

Despite the parameter passed to the Spark jobs being `${KAFKA_INPUT_BROKERS}` this field is actually computed from `kafka_input_brokers_alias` and `secure_kafka` in the following manner:
- The value of `kafka_input_brokers_alias` is evaluated in the DNS and it will be resolved to all the FQDNs behind said alias.
- Every FQDN will be appended with a colon, and the corresponding port based on whether the connection is secure:
  - 9092 for unsecure connections (PLAINTEXT).
  - 9093 for secure connections (SASL_SSL).
- Then all the FQDNs with their ports attached are joined by commas, and the resulting value is given to `${KAFKA_INPUT_BROKERS}`

It is worth noting that:
- If `kafka_input_brokers` is manually defined it will override the one created with the alias and secure options.
  - The validation of the security fields will still take place unless explicitly specified (secure_kafka: false).
- `secure_kafka` is set to true by default in all environments.


#### Example

Below, the contents of a `config/jobs.yml` file using variables and overriding some defaults can be found:

```
---
app_version: 1.6

jobs:
  - job_name: monit-fts-aggregation
    image: "${DOCKER_IMAGE}"
    job_type: "batch"
    cronjob_value: '*/30 * * * * *'
    entry_point: "${ENTRY_POINT}"
    command_line:
      - command: "cd"
        parameters:
        - "/etc/kafka"
      - command: "/usr/lib/spark/bin/spark-submit"
        parameters:
        - "--verbose"
        - "--master yarn"
        - "--deploy-mode cluster"
        - "--principal monitops@CERN.CH"
        - "--keytab /etc/monit/monitops.keytab"
        - "--driver-java-options \"-XX:+UseG1GC -Djava.security.auth.login.config=kafkaclient_jaas.conf\""
        - "--files kafkaclient_jaas.conf,truststore.jks,keystore.jks,keypass,kafka_auth.keytab"
        - "--conf spark.serializer=\"org.apache.spark.serializer.KryoSerializer\""
        - "--conf spark.dynamicAllocation.enabled=true"
        - "--conf spark.shuffle.service.enabled=true"
        - "--conf spark.dynamicAllocation.maxExecutors=16"
        - "--conf spark.dynamicAllocation.minExecutors=1"
        - "--conf spark.dynamicAllocation.initialExecutors=4"
        - "--conf spark.deploy.defaultCores=8"
        - "--conf spark.executor.memory=4g"
        - "--conf spark.executor.extraJavaOptions=\"-XX:+UseG1GC -Djava.security.auth.login.config=kafkaclient_jaas.conf\""
        - "--conf spark.driver.memory=4g"
        - "--conf spark.jars=\"hdfs://${HADOOP_CLUSTER_NAME}/user/monitops/jars-${SPARK_VERSION}/*.jar\""
        - "--conf spark.ui.port=10002"
        - "--conf spark.authenticate=true"
        - "--class ch.cern.monitoring.FTSMainApplication"
        - "/monit/spark-fts-aggregation/spark-fts-aggregation-assembly-${APP_VERSION}.jar"
        - "--truststore-location truststore.jks"
        - "--truststore-password `cat keypass`"
        - "--keystore-location keystore.jks"
        - "--keystore-password `cat keypass`"
        - "--key-password `cat keypass`"
        - "--batch true"
        - "--recovery false"
        - "--input-brokers-url ${KAFKA_INPUT_BROKERS}"
        - "--input-topic fts_raw_complete"
        - "--output-brokers-url ${KAFKA_OUTPUT_BROKERS}"
        - "--output-topic ${KAFKA_TOPIC_PREFIX}fts_agg_complete"
        - "--schema \"hdfs://${SCHEMA_LOCATION}/fts.json\""
        - "--state-schema \"hdfs://${SCHEMA_LOCATION}/fts_state.json\""
        - "--time-window 60"
        - "--watermark-hours 6"
        - "--log-level WARN"
        - "--is-local false"
  - job_name: monit-fts-enrichment
    image: "${DOCKER_IMAGE}"
    job_type: "service"
    entry_point: "${ENTRY_POINT}"
    command_line:
      - command: "cd"
        parameters:
        - "/etc/kafka"
      - command: "/usr/lib/spark/bin/spark-submit"
        parameters:
        - "--verbose"
        - "--master yarn"
        - "--deploy-mode cluster"
        - "--principal monitops@CERN.CH"
        - "--keytab /etc/monit/monitops.keytab"
        - "--driver-java-options \"-XX:+UseG1GC -Djava.security.auth.login.config=kafkaclient_jaas.conf\""
        - "--files kafkaclient_jaas.conf,truststore.jks,keystore.jks,keypass,kafka_auth.keytab"
        - "--conf spark.serializer=\"org.apache.spark.serializer.KryoSerializer\""
        - "--conf spark.dynamicAllocation.enabled=true"
        - "--conf spark.shuffle.service.enabled=true"
        - "--conf spark.dynamicAllocation.maxExecutors=16"
        - "--conf spark.dynamicAllocation.minExecutors=1"
        - "--conf spark.dynamicAllocation.initialExecutors=4"
        - "--conf spark.deploy.defaultCores=8"
        - "--conf spark.executor.memory=4g"
        - "--conf spark.executor.extraJavaOptions=\"-XX:+UseG1GC -Djava.security.auth.login.config=kafkaclient_jaas.conf\""
        - "--conf spark.driver.memory=3g"
        - "--conf spark.yarn.jars=\"hdfs://${HADOOP_CLUSTER_NAME}/user/monitops/jars-${SPARK_VERSION}/*\""
        - "--conf spark.ui.port=$PORT0"
        - "--conf spark.authenticate=true"
        - "--class ch.cern.monitoring.FTSMainApplication"
        - "/monit/spark-fts-aggregation/spark-fts-aggregation-assembly-${APP_VERSION}.jar"
        - "--truststore-location truststore.jks"
        - "--truststore-password `cat keypass`"
        - "--keystore-location keystore.jks"
        - "--keystore-password `cat keypass`"
        - "--key-password `cat keypass`"
        - "--batch false"
        - "--recovery false"
        - "--input-brokers-url ${KAFKA_INPUT_BROKERS}"
        - "--input-topic fts_raw_complete"
        - "--starting-offset latest"
        - "--output-brokers-url ${KAFKA_OUTPUT_BROKERS}"
        - "--output-topic ${KAFKA_TOPIC_PREFIX}fts_enr_complete"
        - "--checkpoint hdfs://${CHECKPOINT_LOCATION}/spark-fts-aggregation"
        - "--schema \"hdfs://${SCHEMA_LOCATION}/fts.json\""
        - "--state-schema \"hdfs://${SCHEMA_LOCATION}/fts_state.json\""
        - "--watermark-hours 48"
        - "--job-ttl-ms 86400000"
        - "--log-level WARN"
        - "--is-local false"
```
