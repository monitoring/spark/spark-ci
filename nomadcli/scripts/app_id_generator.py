import click


@click.command()
@click.option('--app-id', help="The base application id")
@click.option('--environment', help="The environment for deployment")
def main(app_id, environment):
    result_app_id = app_id
    if environment == 'dev':
        result_app_id = str(app_id) + "-dev"
    print(result_app_id)


if __name__ == '__main__':
    main()
