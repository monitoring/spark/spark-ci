import re
import socket
from abc import ABC, abstractmethod

import yaml


def _load_yaml_file(path):
    with open(path) as file:
        content = yaml.load(file, Loader=yaml.FullLoader)

    return content


def _generate_command_line(commands, defaults, job_config):
    job_commands = []
    for command in commands:
        command_entrypoint = command['command']
        command_parameters = command['parameters']

        # Check if the command that we are processing is 'spark-submit' or not.
        # Only if yes we inject the queue name.
        if "spark-submit" in command_entrypoint:
            queue = job_config['queue'] if 'queue' in job_config else defaults['queue']
            command_parameters[:0] = [f"--queue {queue}"]

        job_commands.append(f" {command_entrypoint} {' '.join(command_parameters)}")

    return ' && '.join(job_commands)


def _generate_jobs(config, defaults, environment):
    for job in config['jobs']:
        commands = []
        commands.extend(defaults['command_line'])
        commands.extend(job['command_line'])
        job['entry_point'] = _generate_command_line(commands, defaults, job)
        job['hadoop_cluster'] = defaults['hadoop_cluster_source']
        job['environment'] = environment
        if environment == 'stable':
            job['job_name'] = 'monit-' + job['job_name']
        else:
            job['job_name'] = 'monit-' + job['job_name'] + '-' + environment

    return config['jobs']


def _override_defaults_with_config(config, defaults, env):
    for key, value in config.items():
        if key == 'jobs':
            defaults['jobs'] = _generate_jobs(config, defaults, env)
        else:
            defaults[key] = value

    return defaults


def _apply_variables_to_configuration(variables, configuration):
    """Replaces the values in the configuration with the values in the variables parameter with the same key.
       Note that if a key is present in the variables but not in the configuration, it will be ignored

       :arg configuration A configuration in key/value pairs
       :arg variables Key/value pairs to replace in the configuration"""
    for key, value in variables:
        if key in configuration:
            configuration[key] = value

    return configuration


def _replace_variables(config, variables):
    replacement_config = config
    for key, value in variables.items():
        replacement_config = replacement_config.replace(f"${{{key.upper()}}}", str(value))

    return replacement_config


def resolve_aliases(config):
    port = '9093' if config['secure_kafka'] is True else '9092'

    for key, value in list(config.items()):
        if re.search("alias", key):
            host_port_list = []
            ips = socket.gethostbyname_ex(value)[2]
            for ip in ips:
                host_port_list.append(socket.gethostbyaddr(ip)[0] + ":" + port)

            split_key = str(key).split("_")
            split_key.remove("alias")
            key_without_alias = "_".join(split_key)

            if key_without_alias not in config:  # If kafka_xxx_brokers is defined manually, it overrides the alias
                config[key_without_alias] = ','.join(host_port_list)


class ConfigGenerator(ABC):
    def main(self, environment, app_config_path, variables):
        skeleton = _load_yaml_file(f'../resources/skeletons/jobs.yml')
        environment_defaults = _load_yaml_file(f'../resources/defaults/{environment}.yaml')
        application_config = _load_yaml_file(app_config_path)

        app_defaults_config = _override_defaults_with_config(application_config, environment_defaults, environment)  # Config in the job overrides the defaults
        merged_config = _apply_variables_to_configuration(variables, app_defaults_config)  # Variables specified with --variable override the config
        self._add_specific_config(merged_config)  # Platform specific configuration
        resolve_aliases(merged_config)
        job_config = _apply_variables_to_configuration(merged_config.items(), skeleton)  # Variables in the skeleton are replaced with their value in the config

        print(_replace_variables(yaml.dump(job_config), merged_config))

    @abstractmethod
    def _add_specific_config(self, merged_app_config):
        pass
