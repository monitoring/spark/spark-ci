import click
import os


from config_generator import ConfigGenerator


class NomadConfigGenerator(ConfigGenerator):
    def __init__(self, docker_image):
        self.docker_image = docker_image

    def _add_specific_config(self, merged_app_config):
        merged_app_config['docker_image'] = self.docker_image


@click.command()
@click.option('--environment', help="The environment for deployment")
@click.option('--app-config-path', help="The path for the application config")
@click.option('--docker-image', help="The path for the docker image to configure")
@click.option('--variable', 'variables', type=(str, str), multiple=True,
              help="Key/Value pair to define a variable. Takes precedence over those defined in the config file")
def main(environment, app_config_path, docker_image, variables):
    NomadConfigGenerator(docker_image).main(environment, app_config_path, variables)


if __name__ == '__main__':
    main()
