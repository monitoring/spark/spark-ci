import click
import yaml
import warnings


def _load_application_config(app_config_path):
    with open(app_config_path) as app_yaml:
        app_config = yaml.load(app_yaml, Loader=yaml.FullLoader)
    return app_config


def _load_build_application_version(sbt_build):
    app_version = ""
    with open(sbt_build) as build_file:
        for line in build_file:
            if line.startswith("version"):
                app_version = line.split(":=")[1].strip().replace('"', '')

    return app_version


def _check_application_version_matches_build(app_version, sbt_build):
    build_version = _load_build_application_version(sbt_build)
    if str(app_version) != str(build_version):
        raise ValueError("App version and build version don't match")


def _check_kafka_brokers_definition(params):
    if not all(item in str(params) for item in ["--input-brokers-url", "--output-brokers-url"]):
        raise ValueError("Please submit kafka brokers")
    for param in params:
        if param.startswith("--input-brokers-url"):
            if "${KAFKA_INPUT_BROKERS}" not in param:
                raise ValueError("Kafka brokers can't be hardcoded")
        elif param.startswith("--output-brokers-url"):
            if "${KAFKA_OUTPUT_BROKERS}" not in param:
                raise ValueError("Kafka brokers can't be hardcoded")


def _check_kafka_topics_definition(params):
    if not all(item in str(params) for item in ["input-topic", "output-topic"]):
        raise ValueError("Please submit kafka topics")
    for param in params:
        if "output-topic" in param:
            if "${KAFKA_TOPIC_PREFIX}" not in param:
                raise ValueError("Output topic can't be hardcoded")


def _check_kafka_secure_access(params):
    required_secure_fields = ["--driver-java-options", "--conf spark.executor.extraJavaOptions", "--files", "--truststore-location",
                              "--truststore-password", "--keystore-location", "--keystore-password", "--key-password"]

    if not all(field in str(params) for field in required_secure_fields):
        raise ValueError("At least one of the following required parameters is missing: " + " ".join(required_secure_fields))

    for param in params:
        if param.startswith("--driver-java-options") or "spark.executor.extraJavaOptions" in param:
            if "java.security.auth.login.config" not in param:
                raise ValueError("The --driver-java-options and --conf spark.executor.extraJavaOptions need to include:"
                                 " -Djava.security.auth.login.config={path to kafkaclient_jaas.conf}")


def _check_application_name(parameters):
    if "--name ${NOMAD_JOB_NAME}" not in str(parameters):
        raise ValueError("The --name ${NOMAD_JOB_NAME} argument needs to be specified for nomad poststop tasks.")


def _check_command_line(command_line, check_kafka, secure_kafka):
    for command in command_line:
        if "spark-submit" in command['command']:
            if check_kafka:
                _check_kafka_brokers_definition(command['parameters'])
                _check_kafka_topics_definition(command['parameters'])
                if secure_kafka:
                    _check_kafka_secure_access(command['parameters'])
            _check_application_name(command['parameters'])


def _check_spark_monit_utilities_version(sbt_build):
    with open(sbt_build) as build_file:
        for line in build_file:
            if all(item in line for item in ["monit-utilities", "%"]):
                dependency = line.split("%")
                version = dependency[len(dependency) - 1].strip(' "')

                if not isinstance(version, int):
                    warnings.warn("Warning: using a non-official version of spark monit utilities")
                else:
                    if int(version) < "3.0":
                        raise ValueError("Spark Monit Utilities >= 3.0 is required to access Kafka securely")


def _check_application_config(app_config, sbt_build, check_kafka):
    _check_application_version_matches_build(
        app_config['app_version'],
        sbt_build)

    secure_kafka = app_config["secure_kafka"] if "secure_kafka" in app_config else True
    _check_command_line(app_config['command_line'], check_kafka, secure_kafka)
    if secure_kafka:
        _check_spark_monit_utilities_version(sbt_build)


@click.command()
@click.option('--app-config-path', help="The path for the application config")
@click.option('--sbt-build', help="The path for the sbt build file")
@click.option('--check-kafka', default='true', help="Whether to perform validation for Kafka parameters")
def main(app_config_path, sbt_build, check_kafka):
    check_kafka = check_kafka.lower() == 'true'
    application_config = _load_application_config(app_config_path)

    _check_application_config(application_config, sbt_build, check_kafka)
    return 0


if __name__ == '__main__':
    main()
