job "{{job.job_name}}" {

  {% if job.nomad_datacenters is defined and job.nomad_datacenters %}
  datacenters = ["{{job.nomad_datacenters|join('", "') }}"]
  {% else %}
  datacenters = ["{{defaults.nomad_datacenters|join('", "') }}"]
  {% endif %}

  type = "{{job.job_type}}"

  {% if job.cronjob_value is defined and job.cronjob_value %}
  periodic {
    cron    = "{{job.cronjob_value}}"
    prohibit_overlap = true
    time_zone = "Europe/Zurich"
  }
  {% endif %}

  ###
  # node_class we can overide at the level of the job
  {% if job.node_class is defined and job.node_class %}
  constraint {
    attribute = "${node.class}"
    value     = "{{job.node_class}}"
  }
  {% elif defaults.node_class is defined and defaults.node_class %}
  constraint {
    attribute = "${node.class}"
    value     = "{{defaults.node_class}}"
  }
  {% endif %}
  {% if (job.environment == "qa") %}
  constraint {
    attribute = "${meta.default_env}"
    value     = "qa"
  }
  {% endif %}
  
  meta {
    run_uuid = "${uuidv4()}"
  }

  spread {
    attribute = "${attr.unique.hostname}"
  }
  
  group "spark" {

    count = 1

    network {
      port "ui" {
        to = 10002
      }
      port "spark" {
        to = 5001
      }
    }

    task "client" {

      driver = "docker"

      template {
        destination = "local/resolv.conf"
        left_delimiter = "@#$%^"
        right_delimiter = "^%$#@"
        data = <<EOH
# Using host local DNS caching server for resolution
search cern.ch
nameserver 172.17.0.1
EOH
      }
      template {
        destination = "local/command.sh"
        left_delimiter = "@#$%^"
        right_delimiter = "^%$#@"
        data = <<EOH
{{job.entry_point}}
EOH
      }

      resources {
        {% if job.nomad_client_mem is defined %}
        memory = {{job.nomad_client_mem}}
        {% else %}
        memory = {{defaults.nomad_client_mem}}
        {% endif %}
      }

      service {

        name = "{{job.job_name}}-ui"
        port = "ui"
        tags = [
          "traefik.enable=true",
          "traefik.http.routers.{{job.job_name}}-ui.entrypoints=websecure",
          "traefik.http.routers.{{job.job_name}}-ui.rule=PathPrefix(`/{{job.job_name}}-ui`)",
          "traefik.http.middlewares.{{job.job_name}}-ui-stripprefix.stripprefix.prefixes=/{{job.job_name}}-ui",
	        "traefik.http.routers.{{job.job_name}}-ui.middlewares={{job.job_name}}-ui-stripprefix",
	        "traefik.http.routers.{{job.job_name}}-ui.tls=true",
	      ]

        check {
          name     = "ui port alive"
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }

      }

      service {

        name = "{{job.job_name}}-spark"
        port = "spark"
        tags = [
          "traefik.enable=true",
          "traefik.http.routers.{{job.job_name}}-spark.entrypoints=web",
          "traefik.http.routers.{{job.job_name}}-spark.rule=PathPrefix(`/{{job.job_name}}-spark`)",
          "traefik.http.middlewares.{{job.job_name}}-spark-stripprefix.stripprefix.prefixes=/{{job.job_name}}-spark",
          "traefik.http.routers.{{job.job_name}}-spark.middlewares={{job.job_name}}-spark-stripprefix",
          "traefik.http.services.{{job.job_name}}-spark.loadbalancer.server.scheme=h2c",
        ]

        check {
          name     = "spark port alive"
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }

      }

      config {

        image = "{{job.image}}"
        {% if (job.environment == "dev") %}
        force_pull = true
        {% endif %}
        ports = ["ui", "spark"]
        command = "/usr/bin/bash"
        args = ["-x", "/tmp/command.sh"]

        auth {
         username = "diaglab"
         password = "{{ "not-defined" | env_override('DIAGLAB_REGISTRY_TOKEN_RO') }}"
        }

        volumes = [
          # TLS
          "/etc/dia/certificates:/etc/dia/certificates/",
          "/etc/ssl/certs/:/etc/ssl/certs/",
          # KRB
          "/etc/krb5.conf:/etc/host_etc/krb5.conf",
          "/etc/krb5.keytab:/etc/host_etc/krb5.keytab",
          # DNS
          "local/resolv.conf:/etc/resolv.conf",
          # Entry point
          "local/command.sh:/tmp/command.sh",
          # Base MONIT config
          "/etc/hadoop:/etc/hadoop",
          "/usr/bin/which:/usr/bin/which",
          "/usr/lib/bigtop-utils:/usr/lib/bigtop-utils",
          "/opt/hadoop:/opt/hadoop",
          "/etc/hadoop/conf:/etc/hadoop/conf",
          "/usr/hdp/spark:/usr/lib/spark",
          "/etc/monit:/etc/monit",
          "/usr/hdp/hadoop:/usr/hdp/hadoop",
          "/etc/spark/conf:/etc/spark/conf",
          "/etc/monit/spark-checkpoints:/etc/monit/spark-checkpoints",
          "/etc/kafka:/etc/kafka",
        ]
      }
    }

    task "{{job.job_name}}-poststop" {
      driver = "raw_exec"
      config {
        command = "sh"
        args = ["-c", "export PATH=$PATH:/usr/hdp/hadoop/bin && source /opt/hadoop/hadoop-setconf.sh {{job.hadoop_cluster}} && kinit -k -t /etc/monit/monitops.keytab monitops@CERN.CH && for app in $(yarn application --list | grep ${NOMAD_JOB_NAME} | awk '{ print $1 }'); do yarn application -kill $app; done"]
      }

      lifecycle {
        hook = "poststop"
        sidecar = false
      }
    }
  }
}
